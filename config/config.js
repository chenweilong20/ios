/**
 * 应用配置
 */
console.log(uni.getStorageSync('ry_appKey'))
console.log(uni.getStorageSync('userId'))
console.log(uni.getStorageSync('ry_token'))
console.log("--------------------------------")
const userList = [
	{
		appkey: uni.getStorageSync('ry_appKey'),
		userId: uni.getStorageSync('userId'),
		token: uni.getStorageSync('ry_token')
	},
]

const config = {
	// base_url: "http://192.168.124.18:8081/crm",
	base_url: "http://jth.sonnynet.com/crm",
	appkey: userList[0].appkey,
	token: userList[0].token,
	userId: '',
	targetId: 'user001',
	navi: '',
	conversationType: 1,
	// 可选择的登录账号
	userList: userList,
	// 发消息时可选择的 targetId, 获取会话列表后会覆盖
	targetIdList: [
		{
			label: 'user001',
			value: 'user001'
		},{
			label: 'user002',
			value: 'user002'
		},{
			label: 'user003',
			value: 'user003'
		},{
			label: 'group001',
			value: 'group001'
		}
	]
}
export default config


export const conversationTypeList = [
	{
		label: '单聊',
		value: 1
	},
	{
		label: '群组',
		value: 3
	},
	{
		label: '聊天室',
		value: 4
	}
]

