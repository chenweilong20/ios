import { apiResquest } from '@/util/http.js'

export const noticeList = (query) => {
	return apiResquest({
		url: '/app/notice/list',
		method: 'GET',
		query: {...query}
	})
}

export const noticeInfo = (query) => {
	return apiResquest({
		url: '/app/notice/info/'+query,
		method: 'GET'
	})
}
