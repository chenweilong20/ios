import { apiResquest } from '@/util/http.js'

export const getDictDataByTypes = (query) => {
	return apiResquest({
		url: '/api/dictdata/getDictDataByTypes',
		method: 'POST',
		query: query
	})
}

export const customerList = (query) => {
	let str = query
	return apiResquest({
		url: "/api/customer/private/app/list",
		method: 'GET',
		query: {...query}
	})
}

export const addCustomer = (query) => {
	return apiResquest({
		url: '/api/customer/private/save',
		method: 'POST',
		query: query
	})
}

export const userList = (query) => {
	return apiResquest({
		url: '/api/user/allList',
		method: 'GET',
		// query: query
	})
}

export const customerInfo = (query) => {
	return apiResquest({
		url: '/api/customer/private/app/info/'+query,
		method: 'GET'
	})
}

export const followlist = (query) => {
	return apiResquest({
		url: '/api/customer/follow/list/'+query,
		method: 'GET'
	})
}

export const addFollow = (query) => {
	return apiResquest({
		url: '/api/customer/follow/save',
		method: 'POST',
		query: query
	})
}

export const getSecurityToken = (query) => {
	return apiResquest({
		url: '/api/common/getSecurityToken',
		method: 'GET'
	})
}

export const analysisInfo = (query) => {
	return apiResquest({
		url: '/api/customer/analysis/info/'+query,
		method: 'GET'
	})
}

export const analysisSave = (query) => {
	return apiResquest({
		url: '/api/customer/analysis/save',
		method: 'POST',
		query: query
	})
}

export const analysisUpdate = (query) => {
	return apiResquest({
		url: '/api/customer/analysis/update',
		method: 'POST',
		query: query
	})
}

export const updateCustomerLabel = (query) => {
	return apiResquest({
		url: '/api/customer/updateCustomerLabel',
		method: 'POST',
		query: query
	})
}

export const orderList = (query) => {
	return apiResquest({
		url: '/api/order/list',
		method: 'GET',
		query: query
	})
}
