import { apiResquest } from '@/util/http.js'

export const logout = (query) => {
	return apiResquest({
		url: '/api/logout',
		method: 'POST',
		query: {...query}
	})
}

export const userInfo = (query) => {
	let str = query
	return apiResquest({
		url: "/api/user/info",
		method: 'GET'
	})
}

export const changePwd = (query) => {
	return apiResquest({
		url: '/api/user/password',
		method: 'POST',
		query: {...query}
	})
}

export const simpleUpdateUser = (query) => {
	return apiResquest({
		url: '/api/user/simpleUpdateUser',
		method: 'POST',
		query: {...query}
	})
}

export const getSecurityToken = (query) => {
	return apiResquest({
		url: '/api/common/getSecurityToken',
		method: 'GET'
	})
}