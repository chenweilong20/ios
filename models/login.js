import { apiResquest } from '@/util/http.js'

export const login = (query) => {
	return apiResquest({
		url: '/api/login',
		method: 'POST',
		query: {...query}
	})
}

export const getRongToken = (query) => {
	let str = query
	return apiResquest({
		url: "/api/common/getRongToken",
		method: 'GET'
	})
}

export const userInfo = (query) => {
	let str = query
	return apiResquest({
		url: "/api/user/info",
		method: 'GET'
	})
}