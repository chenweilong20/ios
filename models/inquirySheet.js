import { apiResquest } from '@/util/http.js'

export const inquiryList = (query) => {
	return apiResquest({
		url: '/api/inquiry/list',
		method: 'GET',
		query: {...query}
	})
}

export const inquiryInfo = (query) => {
	return apiResquest({
		url: '/api/inquiry/info/'+query,
		method: 'GET'
	})
}

export const getDictDataByType = (query) => {
	return apiResquest({
		url: '/api/dictdata/getDictDataByType/'+query,
		method: 'POST'
	})
}

export const getQuoteInfoByInquiryId = (query) => {
	return apiResquest({
		url: '/api/quote/getQuoteInfoByInquiryId/'+query,
		method: 'GET'
	})
}

export const inquiryUpdate = (query) => {
	return apiResquest({
		url: '/api/inquiry/update',
		method: 'POST',
		query: query
	})
}

export const getDictDataByTypes = (query) => {
	return apiResquest({
		url: '/api/dictdata/getDictDataByTypes',
		method: 'POST',
		query: query
	})
}

export const getPosition = (query) => {
	return apiResquest({
		url: '/api/user/listByPosition/'+query,
		method: 'GET'
	})
}

export const transferBusiness = (query) => {
	return apiResquest({
		url: '/api/inquiry/transferBusiness',
		method: 'POST',
		query: query
	})
}

export const inquiryCancel = (query) => {
	return apiResquest({
		url: '/api/inquiry/cancel',
		method: 'POST',
		query: query
	})
}
