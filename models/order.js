import { apiResquest } from '@/util/http.js'

export const getSecurityToken = (query) => {
	return apiResquest({
		url: '/api/common/getSecurityToken',
		method: 'GET'
	})
}

export const orderSave = (query) => {
	return apiResquest({
		url: '/api/order/save',
		method: 'POST',
		query: {...query}
	})
}

export const getDictDataByType = (query) => {
	return apiResquest({
		url: '/api/dictdata/getDictDataByType/'+query,
		method: 'POST'
	})
}

export const orderInfo = (query) => {
	return apiResquest({
		url: '/api/order/info/'+query,
		method: 'GET'
	})
}