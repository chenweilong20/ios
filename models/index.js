import { apiResquest } from '@/util/http.js'

export const getDictDataByType = (query) => {
	return apiResquest({
		url: '/api/dictdata/getDictDataByType/'+query,
		method: 'POST'
	})
}

export const getMyCustomer = (query) => {
	return apiResquest({
		url: '/api/customer/getMyCustomer?query='+query,
		method: 'POST'
	})
}

export const getPosition = (query) => {
	return apiResquest({
		url: '/api/user/listByPosition/'+query,
		method: 'GET'
	})
}

export const getDictDataByTypes = (query) => {
	return apiResquest({
		url: '/api/dictdata/getDictDataByTypes',
		method: 'POST',
		query: query
	})
}

export const inquiryCreate = (query) => {
	return apiResquest({
		url: '/api/inquiry/create',
		method: 'POST',
		query: query
	})
}

export const listStartPlace = (query) => {
	return apiResquest({
		url: '/api/transportprice/listStartPlace',
		method: 'GET'
	})
}

export const listEndPlace = (query) => {
	return apiResquest({
		url: '/api/transportprice/listEndPlace',
		method: 'GET'
	})
}

export const transportpriceList = (query) => {
	return apiResquest({
		url: '/api/transportprice/list',
		method: 'GET',
		query: {...query}
	})
}

export const transportpriceInfo = (query) => {
	return apiResquest({
		url: '/api/transportprice/info/'+query,
		method: 'GET'
	})
}