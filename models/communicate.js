import { apiResquest } from '@/util/http.js'

export const userInfo = (query) => {
	let str = query
	return apiResquest({
		url: "/api/user/info",
		method: 'GET'
	})
}

export const rongGroupMessage = (query) => {
	return apiResquest({
		url: '/api/rong/cloud/groupMessage',
		method: 'POST',
		query: query
	})
}

export const cloudMyGroup = (query) => {
	return apiResquest({
		url: '/api/rong/cloud/myGroup',
		method: 'GET'
	})
}

export const getDictDataByTypes = (query) => {
	return apiResquest({
		url: '/api/dictdata/getDictDataByTypes',
		method: 'POST',
		query: query
	})
}

export const inquiryInfo = (query) => {
	return apiResquest({
		url: '/api/inquiry/info/'+query,
		method: 'GET'
	})
}
